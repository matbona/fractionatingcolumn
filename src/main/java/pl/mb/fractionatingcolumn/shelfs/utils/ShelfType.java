package pl.mb.fractionatingcolumn.shelfs.utils;

public enum ShelfType {

	NORMAL, FEEDER, EVAPORATOR, CONDENSER;
}
