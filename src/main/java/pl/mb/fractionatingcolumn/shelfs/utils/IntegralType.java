package pl.mb.fractionatingcolumn.shelfs.utils;

public enum IntegralType {
    X, M;
}
